start: build
	@docker-compose up

build: clean
	@docker-compose build

clean: 
	@docker-compose kill && docker-compose rm -fv

.PHONY: qa

qa:
	@docker-compose exec flasky-tests sh