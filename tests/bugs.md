# Found Bugs
> Just a glimpse of bugs identified during implementation of auto-test. My goal was not to find bugs!

### BG1 - "sqlite3" library not found in Alpine Package Manager
>The initial [Dockerfile](../app/Dockerfile#6) seems to use sqlite3 which is not found in Alpine Package Manager

**Steps to reproduce:**
* build initial Dockerfile using 
  ```
  RUN apk add --no-cache sqlite3
  ```
**Actual Status:**
 ```ruby
 ERROR: unsatisfiable constraints:
  sqlite3 (missing):
    required by: world[sqlite3]
 ```
 > see current Dockerfile with fix! 
 ---


### BG2 - there is no field validation
> i.e. for username, password complexity, length, number, if field contains empty spaces.

### [security]BG3 - with valid Token I can update details or another user
> see 3-spi-register.robot failing test (TDD)

### [security]BG4 - no error handling on API calls on updating user's phone
> if I use special single quote (') server will return 500 Error code.
