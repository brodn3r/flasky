# About
> Describes steps that you need to take in order to run [Robot](http://robotframework.org) tests

> Tests can be executed on your machine but also on containers (recommended approach). In order to do that, I've re-structured a little bit the project (more information on [structure](#structure) section bellow)

# `!` Prerequisites

> tested with the following libraries on macOS
* Python 3.7.2 
* Robot Framework 3.1.2
* docker
* docker-compose

# Structure

```ruby
├── README.md  # initial readme
├── app             # new location of demo_app
│   ├── Dockerfile  
│   ├── demo_app
│   │   ├── (...)
│   ├── requirements.txt
│   └── run.sh # updated to work in containers
├── docker-compose.yml # bring app image and test image together in one shot
└── tests
    ├── Dockerfile # creates the QA environment with all libraries
    ├── README.md  # this readme file
    ├── requirements.txt  # pertaining to test environment
    └── suites  # self explanatory
        ├── 1-ui-register.robot
        ├── common.robot
        └── 2-api-register.robot  
```

* `app` folder contains the original `demo-app` - related to development/application only (this will produce the application `flasky`[![Docker Repository on Quay](https://quay.io/repository/paulbrodner/flasky/status "Docker Repository on Quay")](https://quay.io/repository/paulbrodner/flasky) image)
    
* `tests` folder containing code related to QA (this will produce the test environment Docker image where we can actually run tests: `flasky-tests`[![Docker Repository on Quay](https://quay.io/repository/paulbrodner/flasky-tests/status "Docker Repository on Quay")](https://quay.io/repository/paulbrodner/flasky-tests) )    
* `docker-compose.yml` file that will automatically build and start the application and test environment in one task.
>`!` for UI tests I am using selenium-standalone, check the image in compose.

# Usage
* start the app and test environment  
  ```shell
  $ make 
  ```
  > this will build the images and start the app with test environment

* connect to test environment
  ```shell
  $ make qa
  ```
  > you will see in container all [tests/suites](./suites) files

* execute tests
  ```shell
  $ /tests # robot *-register.robot  
  ```
* cleanup everything
  ```shell
  $ make clean
  ```

>If you want to execute the tests on your machine, update the `SERVER` value in [suites/common.robot](./suites/common.robot) to point to "http://127.0.0.1:8080:

# Found Bugs
* check [bugs.md](./bugs.md)
