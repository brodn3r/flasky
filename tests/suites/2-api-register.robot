*** Settings ***
Documentation  Implementing acceptance criterias as Workflow tests
...             As an API Consumer I can:
...             1) Review users registered in system
...             2) If authenticated I can get personal information of users
...             3) If authenticated I can update personal information of users
Resource          common.robot
Library           REST          http://${SERVER}/api 

*** Variables ***

&{BASIC_AUTH}       Authorization=Basic ${BASE64_CREAD} 

*** Test Cases ***
Using API I cannot see user's personal details is am am unauthenticated
     GET            /users/1
     Integer        response status           401    
     String         response body message   Token authentication required
     [Teardown]     Output  response body
    
Using API I can see user's personal details if I am authenticated
     Set headers   ${BASIC_AUTH}      
     GET            /auth/token
     ${token}=      Output         response body token     
     Set Headers    {"Token": "${token}"}
     Set Headers    {"Content-Type":"application/json"}                  
     GET            /users/${USERNAME}           
     Integer        response status           200    
     String         response body message   retrieval succesful
     String         response body payload firstname         ${FIRSTNAME}
     String         response body payload lastname          ${LASTNAME}
     String         response body payload phone             ${PHONENUMBER}
     Output         response body
     
Using API I update my personal details
     Set headers   ${BASIC_AUTH}      
     GET            /auth/token
      ${token}=      Output         response body token 
     Set Headers    {"Token": "${token}"}
     Set Headers    {"Content-Type":"application/json"}  
     PUT            /users/${USERNAME}       {"phone":"22"}
     Output         response body
     String         response body message         Updated
     GET            /users/${USERNAME}       
     String         response body payload phone             22
     Output         response body

Using API I cannot updated critical username field
     Set headers   ${BASIC_AUTH}      
     GET            /auth/token
      ${token}=      Output         response body token 
     Set Headers    {"Token": "${token}"}
     Set Headers    {"Content-Type":"application/json"}  
     PUT            /users/${USERNAME}       {"username":"ok"}
     String         response body message         Field update not allowed
     