*** Settings ***
Documentation     Common resources for our demo_app application...

*** Variables ***
${GRID_URL}       http://firefox:4444/wd/hub
${SERVER}         flasky-app:8080
${BROWSER}        firefox


${USERNAME}       pbrodner
${PASSWORD}       myPassword
# echo -n 'pbrodner:myPassword' | base64
${BASE64_CREAD}   cGJyb2RuZXI6bXlQYXNzd29yZA==
${FIRSTNAME}      Paul
${LASTNAME}       Brodner
${PHONENUMBER}    0740000123