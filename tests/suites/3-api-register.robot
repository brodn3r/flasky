*** Settings ***
Documentation  Implementing acceptance criterias as Workflow tests
...             As an API Consumer I can:
...             1) Review users registered in system
...             2) If authenticated I can get personal information of users
...             3) If authenticated I can update personal information of users
Resource          common.robot
Library           REST          http://${SERVER}/api 

*** Variables ***

&{BASIC_AUTH}       Authorization=Basic ${BASE64_CREAD} 

*** Test Cases ***
     
Using API I cannot updated other's users details using valid Token
     Set headers   ${BASIC_AUTH}      
     GET            /auth/token
      ${token}=      Output         response body token 
     Set Headers    {"Token": "${token}"}
     Set Headers    {"Content-Type":"application/json"}       
     PUT            /users/test       {"phone":"testing this"}
     Output         response body
     String         response body status         FAILED
