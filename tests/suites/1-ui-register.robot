*** Settings ***
Documentation  Implementing acceptance criterias
...             As an end user I can:
...             1) Register through web portal
...             2) Review my user information from the main view
Resource          common.robot
Library           SeleniumLibrary

*** Variables ***
${LOGIN URL}      http://${SERVER}/login
${REGISTER URL}   http://${SERVER}/register

*** Test Cases ***
I can successfully register through web portal    
    Open Browser in Grid        url=${REGISTER URL}
    Title Should Be             Register - Demo App
    Input Text                  username        ${USERNAME}
    Input Text                  password        ${PASSWORD}
    Input Text                  firstname       ${FIRSTNAME}
    Input Text                  lastname        ${LASTNAME}
    Input Text                  phone           ${PHONENUMBER}
    Click Button                Register        
    Title Should Be             Log In - Demo App    
    [Teardown]                  Close Browser        

I cannot register twice with the same data    
    Open Browser in Grid        url=${REGISTER URL}
    Input Text                  username        ${USERNAME}
    Input Text                  password        ${PASSWORD}
    Input Text                  firstname       ${FIRSTNAME}
    Input Text                  lastname        ${LASTNAME}
    Input Text                  phone           ${PHONENUMBER}
    Click Button                Register
    Page Should Contain Element    xpath: //div[contains(@class,'flash') and contains(text(),'User ${USERNAME} is already registered.')]
    [Teardown]                  Close Browser

I can successfully register another new user through web portal    
    Open Browser in Grid        url=${REGISTER URL}    
    Input Text                  username        test
    Input Text                  password        test
    Input Text                  firstname       test
    Input Text                  lastname        test
    Input Text                  phone           12345678
    Click Button                Register        
    Title Should Be             Log In - Demo App    
    [Teardown]                  Close Browser  

I can review my user information from the main view    
    Open Browser in Grid        url=${LOGIN URL}
    Title Should Be             Log In - Demo App
    Input Text                  username    ${USERNAME}
    Input Text                  password   ${PASSWORD}
    Click Button                Log In
    Title Should Be             User Information - Demo App
    Table Row Should Contain    content     2   Username ${USERNAME}
    Table Row Should Contain    content     3   First name ${FIRSTNAME}
    Table Row Should Contain    content     4   Last name ${LASTNAME}
    Table Row Should Contain    content     5   Phone number ${PHONENUMBER}
    [Teardown]                  Close Browser

*** Keywords ***
Open Browser in Grid
    [Arguments]    ${url}
    Open Browser        ${url}    ${BROWSER}   remote_url=${GRID_URL}
